# Zhongheng Liu

Web and software developer proficient in TypeScript and Java Spring Boot framew
ork. Linux system enthusiast and self-proclaimed preacher of FOSS philosophy.

## Some notable projects

- `stvnliu/pdf-trim` is a small utility to remove pages from a PDF document
before the page number of the actual content begins.
- `stvnliu/byron-college-coding-club` contains tutorial files written in Python
script to be used in the Byron College Coding Club.
- `stvnliu/epqapi` and `stvnliu/epq-web` are counterparts to a chat server
written in Java and TypeScript that is the artefact of an EPQ.
- `stvnliu/KindleRSS` forks `Ate329/KindleRSS`, which implements an RSS
subscription feature to automatically translate articles from a top-level RSS
source to an EPUB file that can be viewed on a device like a Kindle.
- `stvnliu/nixos-dotfiles` is a consistently updating repository containing
the personal configuration of my NixOS laptop, designed to be as reusable as
physically possible.
- `stvnliu/proteinpedia-next` is a web app to create a unified interface to per
form CRUD updates on info in relation to proteins used in biochemistry. In acti
ve development.

## Interests

- Web development
- Algorithms and Computer Science
- Linux system administration

## Skills

- "How can privay and web inter-connectivity be ensured in self-hosted online
chat applications" - an Edexcel Extended Project Qualification
  - Completed as of May 25th 2024.
  - Grading yet to be received from Edexcel.
- (Ongoing) IBM Full Stack Developer Professional Certificate on Coursera
- (Ongoing) Computer Science: An Interdisciplinary Approach - Princeton
University on Coursera

## Learn more about me

My personal [website](https://stvnliu.gitlab.io)
Contact me via [email](mailto:z.liu@outlook.com.gr) for other requests!
:)
